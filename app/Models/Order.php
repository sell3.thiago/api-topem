<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;
    public $table = 'orders';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'client_id',
        'product_id',
        'num_orden',
        'quantity',
        'observation',
        'created_user'
    ];


    public function products() {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function client() {
        return $this->belongsTo(Client  ::class, 'client_id', 'id');
    }
}
