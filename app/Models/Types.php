<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Types extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'types';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'type',
        'description',
    ];
}
