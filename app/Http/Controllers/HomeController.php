<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    // THIS FUNCTION RETURN GENERAL INFORMATION USING COUNTERS
    public function counterInformation() {
        $products = Product::count();
        $clients = Client::count();
        $orders = Order::distinct('num_orden')->count();
        
        return response()->json([
            'productos' => $products,
            'clientes' => $clients,
            'ordenes' => $orders,
        ]);
    }

    // THIS FUNCTION RETURN QUANTITY OF ORDER FOR DATE
    public function grapsOrdersMonth() {
        $records = DB::table('orders')
                    ->selectRaw('COUNT(id) as Total, created_at as Fecha')
                    ->groupBy('created_at')
                    ->get();

        return response()->json($records);
    }
    // THIS FUNCTION RETURN ALL INFO FOR SELECTS OF FORMS
    public function allInformationList() {
        $clients  = Client::all();
        $products = Product::all();

        return response()->json([
            'clientes'  => $clients,
            'productos' => $products 
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
