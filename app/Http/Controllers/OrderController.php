<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Order::with(['products', 'client'])->get();
        foreach ($records as $value) {
            $value->total = ( $value->quantity * $value->products->price_sale );
        }
        return response()->json($records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quantity = Order::count();
        // Log::info('OrderController '.$request->all());
        // IF EXIST ORDERS SEARCH THE LAST ORDER
        if($quantity > 0) 
        {
            $x = Order::select('num_orden')->limit(1)->orderBy('id', 'DESC')->first();
            $last = intval($x->num_orden + 1);
        } else {
            // ELSE ASIGN FIRST NUM_ORDER
            $last = 0;
        }
        DB::beginTransaction();
        try {
            foreach($request->products as $value) 
            {
                $response[] = Order::create([
                    'client_id'     => $request->client,
                    'product_id'    => $value['product'],
                    'num_orden'     => $last ,
                    'quantity'      => $value['quantity'],
                    'observation'   => $value['observation'] ? $value['observation']  : 'N/A' ,
                    'created_user'  => $request->created_user
                ]);
            }
            $response = [
                'message' => 'success',
                'response' => $response
            ];
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $response = array(
                'message' => 'failed',
                'error' => $th->getMessage(),
            );
        }
        
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $records = Order::with(['products', 'client' ])->whereId($id)->get();
        return response()->json($records);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
