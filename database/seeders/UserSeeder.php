<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // USUARIO POR DEFAULT
        User::create([
            'name' => 'Thiago Lopez',
            'email' => 'sell3.thiago@gmail.com',
            'password' => Hash::make('Thiagox3.')
        ]);
    }
}
