<?php

namespace Database\Seeders;

use App\Models\Types;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Types::create([
            'type' => 'Higiene',
            'description' => 'Higiene personal'
        ]);
        Types::create([
            'type' => 'Verduras',
            'description' => 'Verduras'
        ]);
        Types::create([
            'type' => 'Escolar',
            'description' => 'Productos de uso escolar'
        ]);
        Types::create([
            'type' => 'Granos',
            'description' => 'Granos'
        ]);
    }
}
