<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'type_id'           => 4,
            'code' => 'AE23W',
            'name' => 'Avena',
            'description'       => 'Avena ',
            'price_purchase'    => '2000',
            'price_sale'        => '2200',
            'quantity'          => '20'
        ]);
        Product::create([
            'type_id'           => 2,
            'code' => 'AG35K',
            'name' => 'Aguacate',
            'description'       => 'Aguacate fresco',
            'price_purchase'    => '3000',
            'price_sale'        => '3500',
            'quantity'          => '70'
        ]);
        Product::create([
            'type_id'           => 2,
            'code' => 'BA4K1',
            'name' => 'Banano',
            'description'       => 'Banano verde o maduro',
            'price_purchase'    => '300',
            'price_sale'        => '500',
            'quantity'          => '200'
        ]);
        Product::create([
            'type_id'           => 1,
            'code' => 'C3P5D',
            'name' => 'Cepillo de dientes',
            'description'       => 'Cepillo de dientes',
            'price_purchase'    => '1500',
            'price_sale'        => '2000',
            'quantity'          => '130'
        ]);
        Product::create([
            'type_id'           => 1,
            'code' => 'H35AK',
            'name' => 'Pasta de dientes Colgate',
            'description'       => 'Pasta de dientes',
            'price_purchase'    => '2300',
            'price_sale'        => '3000',
            'quantity'          => '30'
        ]);
        Product::create([
            'type_id'           => 1,
            'code' => 'H35AK',
            'name' => 'Pasta de dientes ORAL B',
            'description'       => 'Pasta de dientes',
            'price_purchase'    => '3400',
            'price_sale'        => '3500',
            'quantity'          => '30'
        ]);
        Product::create([
            'type_id'           => 1,
            'code' => '',
            'name' => 'Papel higiénico',
            'description'       => 'Papel higiénico',
            'price_purchase'    => '2000',
            'price_sale'        => '3000',
            'quantity'          => '90'
        ]);
        Product::create([
            'type_id'           => 3,
            'code' => 'C5A8F',
            'name' => 'Cuaderno',
            'description'       => 'Cuaderno',
            'price_purchase'    => '2300',
            'price_sale'        => '2500',
            'quantity'          => '100'
        ]);
        Product::create([
            'type_id'           => 3,
            'code' => 'B908A',
            'name' => 'Boligrafo',
            'description'       => 'Boligrafo',
            'price_purchase'    => '1000',
            'price_sale'        => '1200',
            'quantity'          => '130'
        ]);
        Product::create([
            'type_id'           => 3,
            'code' => 'E5C4D',
            'name' => 'Escuadras',
            'description'       => 'Escuadras',
            'price_purchase'    => '600',
            'price_sale'        => '650',
            'quantity'          => '10'
        ]);

    }
}
