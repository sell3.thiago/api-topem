<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create([
            'cc'   => '13498234',
            'name' => 'Thiago',
            'last_name' => 'Lopez',
            'address'   => 'Montería, Barrio Centro'
        ]);

        Client::create([
            'cc'   => '8423234',
            'name' => 'Manuel A.',
            'last_name' => 'Macea A.',
            'address'   => 'Bello Antioquia'
        ]);
        Client::create([
            'cc'   => '2938423',
            'name' => 'María Fernanda',
            'last_name' => 'Arrieta A',
            'address'   => 'Antioquia, Poblado'
        ]);
        Client::create([
            'cc'   => '2394823',
            'name' => 'Juan A.',
            'last_name' => 'Perez Salcedo',
            'address'   => 'Montería'
        ]);

        Client::create([
            'cc'   => '2398423',
            'name' => 'Andrea',
            'last_name' => 'Valdiri B.',
            'address'   => 'Montelibano, Córdoba'
        ]);

        Client::create([
            'cc'   => '2304821',
            'name' => 'Daniela ',
            'last_name' => 'Vanegas P',
            'address'   => 'Ayapel, Córdoba'
        ]);
    }
}
