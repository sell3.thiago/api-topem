<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            
            $table->foreignId('client_id');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreignId('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->string('num_orden');
            $table->unsignedBigInteger('quantity');
            $table->string('observation');
            $table->string('created_user');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
